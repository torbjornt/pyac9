`AC9.py` contains some methods for reading and processing data from a WetLabs AC-9.
The input should be text files as generated by WetView 5.
I haven't used WV7, so I don't know if that creates the same type of files.

Only tested with Python 3.4.
Depends on `numpy`, `pandas` and `abscorr` from https://bitbucket.org/torbjornt/absorptioncorrections

This code is licensed under the MIT license.