from distutils.core import setup

setup(name='AC9',
      version='0.2',
      description='Reading data from AC-9',
      author='Torbjørn Taskjelle',
      author_email='Torbjorn.Taskjelle@ift.uib.no',
      py_modules=['AC9']
      )
